module AA222

export contour, contourf, meshgrid, ndgrid, xdom, ydom, plot, scatter, arrow, semilogx, optimize, xlim, ylim, subplot, hold, gcf, figure, withfig, legend, @manipulate

import PyPlot: contour, contourf, colorbar, clabel, plot, xlim, ylim, scatter, quiver, semilogx, arrow, xlim, ylim, subplot, hold, gcf, figure, withfig, legend

import Interact: @manipulate

#import Optim: optimize

include("ndgrid.jl")

_xdom = (0.,1.)
_ydom = (0.,1.)

function xdom(xmin, xmax)
    global _xdom
    _xdom = (xmin, xmax)
end

function ydom(ymin, ymax)
    global _ydom
    _ydom = (ymin, ymax)
end

function plot(f::Function, args...)
    x = linspace(_xdom[1], _xdom[2])
    xlim(_xdom[1], _xdom[2])
    plot(x, map(f, x), args...)
end

function semilogx(f::Function, args...)
    x = logspace(log10(_xdom[1]), log10(_xdom[2]))
    xlim(_xdom[1], _xdom[2])
    semilogx(x, map(f, x), args...)
end

function scatter(x::Tuple, args...)
    assert(length(x) == 2)
    scatter(x[1], x[2], args...)
end

function scatter{T}(x::Array{T,1})
    assert(length(x) == 2)
    scatter(x[1], x[2])
end

function scatter{T}(X::Array{T,2})
    if size(X,1) == 2
        scatter(X[1,:]', X[2,:]')
    elseif size(X,2) == 2
        scatter(X[:,1], X[:,2])
    else
        error("Unexpected array size passed to scatter")
    end
end

function plot{T}(X::Array{T,2})
    if size(X,1) == 2
        plot(X[1,:]', X[2,:]')
    elseif size(X,2) == 2
        plot(X[:,1], X[:,2])
    else
        error("Unexpected array size passed to scatter")
    end
end


function scatter(f::Function, domain::AbstractArray{Int64,1}, args...)
    scatter(domain, map(f, domain), args...)
end

function arrow(x::Vector, dx::Vector)
    assert(length(x) == 2 && length(dx) == 2)
    i1 = ones(Int, 2)
    i2 = 2 * ones(Int, 2)
    quiver(x[i1], x[i2], dx[i1], dx[i2])
end

function arrow(x::Tuple, dx::Array)
    assert(length(x) == 2 && length(dx) == 2)
    i1 = ones(Int, 2)
    i2 = 2 * ones(Int, 2)
    quiver(x[i1], x[i2], dx[i1], dx[i2])
end

contour(f::Function, args...) = contour(f, linspace(_xdom[1], _xdom[2]), linspace(_ydom[1], _ydom[2]), args...)

contourf(f::Function, args...) = contourf(f, linspace(_xdom[1], _xdom[2]), linspace(_ydom[1], _ydom[2]), args...)

function scalarize(x::Number)
    return x
end

function scalarize(x)
    assert(length(x) == 1)
    return x[1]
end

function contour(f::Function, X::AbstractVector, Y::AbstractVector, args...)
    (x, y) = meshgrid(X, Y)
    F = (x, y) -> scalarize(f([x, y]))
    c = contour(x, y, map(F, x, y), args...)
    clabel(c)
    xlim(_xdom[1], _xdom[2])
    ylim(_ydom[1], _ydom[2])
end

function contourf(f::Function, X::AbstractVector, Y::AbstractVector, args...)
    xlim(_xdom[1], _xdom[2])
    ylim(_ydom[1], _ydom[2])
    (x, y) = meshgrid(X, Y)
    F = (x, y) -> scalarize(f([x, y]))
    c = contourf(x, y, map(F, x, y), args...)
    colorbar()
end

#optimize(f::Function, x::Number, args...) = optimize(x -> f(x[1]), [float64(x)], args...)

end # module
